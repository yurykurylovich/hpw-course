'use strict';

const methods = iface => {
  const names = []
  for (const namesKey in iface) {
    const fn = iface[namesKey]
    if (typeof fn === 'function') {
      names.push([namesKey, fn.length])
    }
  }
  return names;
};

console.log(methods({
  m1: x => [x],
  m2: function (x, y) {
    return [x, y];
  },
  m3(x, y, z) {
    return [x, y, z];
  }
}));

module.exports = { methods };
