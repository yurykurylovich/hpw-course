'use strict';

const generateKey = (length, possible) => {
  let resultStr = '';
  const base = possible.length;
  for (let i = 0; i < length; i++) {
    const index = Math.floor(Math.random() * base)
    resultStr += possible[index]
  }
  return resultStr
};

console.log(generateKey(18, 'jkjfkdslsljfsklfjkds'))

module.exports = { generateKey };
