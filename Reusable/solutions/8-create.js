'use strict';

const createUser = function (name, city) {
  return { name, city }
};

console.log(createUser('Yury', 'Minsk'));

module.exports = { createUser };
