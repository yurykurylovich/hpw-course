'use strict';

const average = function (num1, num2) {
  return (num1 + num2) / 2;
};

const square = function (num) {
  return num * num;
}

const cube = function (num) {
  return num ** 3
}

const calculate = function () {
  let result = [];
  for (let i = 0; i < 10; i++) {
    result.push(average(square(i), cube(i)));
  }
  return result;
}
console.log(calculate());

module.exports = { square, cube, average, calculate };
