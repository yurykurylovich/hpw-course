'use strict';

const fn = function (name) {
  console.log(`Hello, ${name}`);
};

module.exports = { fn };
