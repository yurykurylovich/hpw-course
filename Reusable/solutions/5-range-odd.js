'use strict';

const rangeOdd = function (start, end) {
  const len = Math.ceil((end - start) / 2);
  if (len < 0) {
    return [];
  }
  const arr = new Array(len);
  let x = 0;
  for (let i = start; i <= end; i++) {
    if (i % 2 !== 0) {
      arr[x++] = i;
    }
  }
  return arr;
};

console.log(rangeOdd(15, 30));

module.exports = { rangeOdd };
