'use strict';

const range = function (start, end) {
  const len = end - start;
  if (len < 0) {
    return [];
  }
  const arr = new Array(len);
  let x = 0;
  for (let i = start; i <= end; i++) {
    arr[x++] = i;
  }
  return arr;
};

console.log(range(15, 30));

module.exports = { range };
