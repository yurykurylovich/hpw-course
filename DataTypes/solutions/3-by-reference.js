'use strict';

const inc = function (numObj) {
  if (typeof numObj === 'object') {
    return numObj.num + numObj.num;
  }
};
const a = { num: 5 };
const b = inc(a);
console.dir(b);

module.exports = { inc };
