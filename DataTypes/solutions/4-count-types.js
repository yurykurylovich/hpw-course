const arr = [true, 'hello', 5, 12, -200, false, false, 'word'];

const countTypesInArray = arr => {
  const counter = {};
  for (const item of arr) {
    const type = typeof item;
    const count = counter[type] || 0;
    counter[type] = count + 1;
  }
  return counter;
};

console.log(countTypesInArray(arr));
